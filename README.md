
# Inkfish.Umbrella

TODO:

 - Add distillery and configure release mode.
 - Add a sample nginx config to the /rel directory.
 - Calculate total score for assignment when line comments saved.
 - Add grade summary to student course/show
 - Add grade summary to reg/show, with links for staff to view student status.
 - Some implementation of teams; data model depends on it.

## OS Package Dependencies

(Package names for Debian 10)

 - graphicsmagick


